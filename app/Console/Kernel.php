<?php

namespace App\Console;

use App\Services\cronServices\cronServices;
use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\File;
class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // Masukkan Kode Anda Disini
        $schedule->call(function () {
            
        //Pengecekan apakah cronjob berhasil atau tidak
        //Mencatat info log 
        CrLpAppLog::truncate();
        CrLpCourseLog::truncate();
        cronServices::generate()->getCronJob();
        })->{config('cron.env.type_cron')}(config('cron.env.time_cron'));
    
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
