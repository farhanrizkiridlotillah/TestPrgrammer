<?php
namespace App\Helpers\Services;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;
use Modules\Backend\Entities\Services\LogActivity;

class MenuHelpers
{

    public static function activeMenu($route = '')
    {
        try {
            //code...
            $menus = explode('/', Request::path());
            $validate = 0;
            foreach ($menus as $menu) {
                # code...
                if ($menu == $route) {
                    # code...
                    $validate = $validate+1;
                }
            }
            ($validate > 0) ? $status = 'active' : $status ='';    
            return $status;
        } catch (\Throwable $th) {
            //throw $th;
            return $status ='';
        }
    }


    public static function breadcrumbMenu()
    {
        try {
            //code...
            $routes = explode('/', Request::path());
            $breadcrumb =[];
            // $breadcrumb[$index] = [
            //     'url'   => '/backend/',
            //     'name'  => config('landingpage.company.information.short_name'),
            //     'status'=> true,
            // ];
            $index = 0;
            foreach ($routes as $route) {
                # code...
                if ($index == 0) {
                    # code...
                    if ($route == 'backend') {
                        # code...
                        $breadcrumb[$index]['url'] =  '/backend/';
                        $breadcrumb[$index]['name'] =  config('landingpage.company.information.short_name');
                        $breadcrumb[$index]['status'] = true;
                        $index++;
                    }
                    if ($route == 'frontend') {
                        # code...
                        $breadcrumb[$index]['url'] =  '/frontend/';
                        $breadcrumb[$index]['name'] =  config('landingpage.company.information.short_name');
                        $breadcrumb[$index]['status'] = true;
                        $index++;
                    }
                }else {
                    if (($route != 'backend') || ($route == 'frontend')) {
                        # code...
                        if (preg_match("/^[a-zA-Z -]+$/",$route)) {
                            # code...
                            $breadcrumb[$index]['url'] =  $breadcrumb[$index-1]['url'].''.$route.'/';
                            $breadcrumb[$index]['name'] =  ucwords(str_replace('-', ' ', $route));
                            $breadcrumb[$index]['status'] = true;
                            $index++;
                        }else {
                            # code...
                            $breadcrumb[$index]['url'] =  $breadcrumb[$index-1]['url'].''.$route.'/';
                            $breadcrumb[$index]['name'] =  ucwords(str_replace('-', ' ', $route));
                            $breadcrumb[$index]['status'] = false;
                            $index++;
                        }
                    }
                }
            }
            return $breadcrumb;
        } catch (\Throwable $th) {
            //throw $th;
            $routes = explode('/', Request::path());
            $breadcrumb = [0=>[
                'url'   => '/backend/',
                'name'  => config('landingpage.company.information.short_name'),
                'status'=> true,
            ],];
            return $breadcrumb;
        }
    }

}
