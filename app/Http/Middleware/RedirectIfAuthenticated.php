<?php

namespace App\Http\Middleware;

use App\Models\User;
use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  ...$guards
     * @return mixed
     */
    public function handle(Request $request, Closure $next, ...$guards)
    {
        $guards = empty($guards) ? [null] : $guards;

        foreach ($guards as $guard) {
            if (Auth::guard($guard)->check()) {

                $user = User::find(Auth::user()->id);
                if ($user->user_type()->get()->count() > 1) {
                    # code...
                    return redirect('/login/auth-type/');
                }else {
                    if ($user->user_type()->first()->name == 'backend') {
                        # code...
                        return redirect('/backend/beranda');
                    }else {
                        # code...
                        return redirect('/frontend/beranda');
                    }
                }
                // return redirect('/backend/dashboard');
            }
        }

        return $next($request);
    }
}
