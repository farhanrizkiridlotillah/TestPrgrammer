<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransaksiPembelian extends Model
{
    // use HasFactory;
    protected $table = "transaksi_pembelian";
    protected $fillable = [
        'total_harga',
        'created_at',
    ];

    public function transaksi_pembelian()
    {
        return $this->hasOne(TransaksiPembelianBarang::class, 'transaksi_pembelian_id', 'id');
    }
}
