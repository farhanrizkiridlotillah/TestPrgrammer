<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransaksiPembelianBarang extends Model
{
    // use HasFactory;
    protected $table = "transaksi_pembelian_barang";
    protected $fillable = [
        'master_barang_id',
        'transaksi_pembelian_id',
        'jumlah',
        'harga_satuan',
        'created_at',
    ];

    public function master_barang()
    {
        return $this->belongsTo(MasterBarang::class, 'master_barang_id', 'id');
    }

    public function transaksi_pembelian()
    {
        return $this->belongsTo(TransaksiPembelian::class, 'transaksi_pembelian_id', 'id');
    }
}
