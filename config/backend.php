<?php return array (
  'name' => 'Backend',
  'app_environments' => 
  array (
    'ssl' => 
    array (
      'name' => 'https://',
      'updated_by' => 1,
    ),
    'host' => 
    array (
      'name' => 'moodle.websku.com',
      'updated_by' => 1,
    ),
    'domain' => 
    array (
      'activated' => '.com',
      'deactivated' => '.delete',
      'updated_by' => 1,
    ),
    'database' => 
    array (
      'activated' => '_com',
      'deactivated' => '_delete',
      'updated_by' => 1,
    ),
    'seeder_admin' => 
    array (
      'name' => 'admin',
      'username' => 'admin',
      'email' => 'admin@moodlenesia.com',
      'updated_by' => 1,
    ),
    'LOCAL_SECRET' => '78fbb9c21da4f62f8dc8bb24bd078422',
    'IP_PUB' => '127.0.1.4',
    'app_master_sso' => 
    array (
      'domain' => 'https://asnunggul.lan.go.id/simplesaml/module.php/core/frontpage_auth.php',
      'username' => 'admin',
      'password' => '12345678',
      'updated_by' => 1,
    ),
    'app_sso_default' => 
    array (
      'domain' => 'https://asnunggul.lan.go.id/simplesaml/module.php/core/frontpage_auth.php',
      'username' => 'admin',
      'password' => '12345678',
      'updated_by' => 1,
    ),
  ),
  'app_master' => 
  array (
    'domain' => 'moodle.asnunggul.lan.go.id',
    'username' => 'admin',
    'password' => 'Ingenio10m!',
    'updated_by' => 1,
  ),
  'app_master_default' => 
  array (
    'domain' => 'moodle.asnunggul.lan.go.id',
    'username' => 'admin',
    'password' => 'Ingenio10m!',
  ),
  'path' => 
  array (
    'sso' => 
    array (
      'static' => 
      array (
        'metadata_default' => '/var/www/apps.generator.com/config/static_default_conf/sso/metadata_default.conf',
        'config_top_default' => '/var/www/apps.generator.com/config/static_default_conf/sso/config_top_default.conf',
        'config_bottom_default' => '/var/www/apps.generator.com/config/static_default_conf/sso/config_bottom_default.conf',
      ),
      'metadata' => '/var/simplesamlphp/metadata/saml20-sp-remote.php',
      'config' => '/var/simplesamlphp/config/config.php',
    ),
    'vhost' => 
    array (
      'available' => '/etc/nginx/sites-available/',
      'enabled' => '/etc/nginx/sites-enabled/',
    ),
    'base' => 
    array (
      'root' => '/var/www/BaseAppsMoodle/',
      'master' => 
      array (
        'moodledata' => '/var/www/BaseAppsMoodle/Master/basic',
        'sql' => '/var/www/BaseAppsMoodle/Master/moodle_local.sql',
      ),
      'active' => 
      array (
        'moodledata' => '/var/www/BaseAppsMoodle/Active/basic',
        'sql' => '/var/www/BaseAppsMoodle/Active/moodle_local.sql',
      ),
      'backup' => 
      array (
        'moodledata' => '/var/www/BaseAppsMoodle/Backup/',
        'sql' => '/var/www/BaseAppsMoodle/Backup/',
      ),
    ),
    'apps' => '/var/www/moodle/',
    'generator' => '/var/www/apps.generator.com/',
    'moodlesite' => '/var/www/moodle/moodlesites/',
    'moodledata' => '/var/moodledata/',
    'data_backup' => '/var/www/DatabasesMoodle/',
    'mysql' => '/var/lib/mysql/',
  ),
  'feature_code' => 
  array (
    'CODE_FORUM' => 115,
  ),
  'app_master_sso' => 
  array (
    'domain' => 'https://asnunggul.lan.go.id/simplesaml/module.php/core/frontpage_auth.php',
    'username' => 'admin',
    'password' => '12345678',
    'updated_by' => 1,
  ),
);