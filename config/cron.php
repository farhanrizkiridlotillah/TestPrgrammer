<?php return array (
  'name' => 'Cron',
  'env' => 
  array (
    'type_cron' => 'daily',
    'time_cron' => NULL,
    'updated_by' => 1,
    'updated_at' => '15 Dec 2021, 03:12 pm',
  ),
  'type' => 
  array (
    0 => 'everyMinute',
    1 => 'hourly',
    2 => 'daily',
    3 => 'weekly',
    4 => 'monthly',
    5 => 'dailyAt',
  ),
);