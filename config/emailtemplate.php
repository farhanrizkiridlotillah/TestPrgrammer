<?php return array (
  'name' => 'emailtemplate',
  'email_default' => 
  array (
    'image' => 'admin/Application/IMG-EMAILTEMPLATE/ET-2021-12-15-12-19-55.png',
    'subhead' => 'test subject',
    'header' => 'Hallo!',
    'subject' => 'Terima kasih sudah mendaftar akun di ASN Unggul multi tenancy!
Sebelum mulai, kami harus memastikan bahwa ini adalah Anda. 
Klik di bawah ini untuk memverifikasi alamat email Anda.',
    'footer' => 'Anda menerima pesan ini karena alamat email Anda dimasukkan 
saat pendaftaran di situs web   ASN UNGGUL. Jika Anda tidak mendaftar, 
abaikan pesan ini.',
  ),
  'email_defaults' => 
  array (
    'image' => 'admin/Application/IMG-EMAILTEMPLATE/ET-2021-11-25-12-37-02.png',
    'subhead' => 'Register ASN Unggul',
    'header' => 'Halo!',
    'subject' => 'Terima kasih sudah mendaftar akun di ASN UNGGUL!
                    Sebelum mulai, kami harus memastikan bahwa ini adalah Anda. 
                    Klik di bawah ini untuk memverifikasi alamat email Anda.',
    'footer' => 'Anda menerima pesan ini karena alamat email Anda dimasukkan 
                    saat pendaftaran di situs web   ASN UNGGUL. Jika Anda tidak mendaftar, 
                    abaikan pesan ini.',
  ),
);