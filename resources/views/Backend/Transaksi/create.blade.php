@extends('layouts.master')
@push('script-head')
@endpush
@push('title', 'Tambah Data Pembelian Barang')
@push('name-content', 'Tambah Data Pembelian Barang')
@section('content')
{{-- @include('backend::feature.master.component.top-bar') --}}
<div class="grid grid-cols-12 gap-6 mt-8">
    <div class="col-span-12 lg:col-span-12">
        <div class="intro-y col-span-12 md:col-span-8 xl:col-span-8 box p-5 sm:p-20">
            <form action="{{URL('transaksi/pembelian-barang')}}" method="post">
                @csrf
                @method("post")
                <div class="grid grid-cols-12 gap-6 formGroup" id="group">
                    <div class="md:col-span-6">
                        <label>Nama Barang</label>
                        <div class="mt-5">
                            <select id="data_barang" data-placeholder="Select your favorite actors" class="tom-select w-full" name="nama_barang">
                                <option selected disabled>Pilih Data Barang</option>
                                @foreach ($data_barang as $barang )
                                    <option value="{{$barang->id}}">{{$barang->nama_barang}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="md:col-span-3">
                        <div  style="margin-top: 40px">
                            <div class="input-group">
                                <div id="input-group-email" class="input-group-text">Rp. </div>
                                <input type="text" name="harga_satuan" id="harga_barang" class="form-control" value="{{old('harga_satuan')}}" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="md:col-span-3">
                        <div  style="margin-top: 40px">
                            <div class="input-group">
                                <div id="input-group-email" class="input-group-text">jumlah</div>
                                <input type="number" name="qty" id="qty" class="form-control" value="{{old('qty')}}" disabled required>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="mt-5 text-right">
                    <a id="tambah_barang"  class="btn btn-success-soft btn-sm "><i data-feather="plus" class="w-4 h-4"></i></a>
                </div>
    
                <div class="grid grid-cols-12 gap-6">
                    <div class="md:col-span-3">
                        <label>Total Harga</label>
                        <div class="input-group mt-5">
                            <div id="input-group-email" class="input-group-text">Rp. </div>
                            <input type="text" name="total_bayar" id="total_bayar" class="form-control" value="" readonly>
                        </div>
                    </div>
                </div>
    
                <div class="mt-5">
                    <button class="btn btn-success btn-sm" type="submit">
                        Simpan Data
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script>
     $(document).ready(function () {
        $('#data_barang').change(function (evt) {
            $('#qty').prop("disabled", false);
            // $('#button_simpan').prop("disabled", false);
            var id = evt.target.value;

            $.get('/transaksi/data-barang?id=' + id, function (data) {
                console.log(data);
                $('#harga_barang').val(data[0].harga_satuan);
            });
        });

        $('#qty').keyup(function (e) {
            var harga_barang = $('#harga_barang').val();
            var get_jumlah = $('#qty').val();
            $("#total_bayar").val(harga_barang * get_jumlah);
        });

    });
</script>