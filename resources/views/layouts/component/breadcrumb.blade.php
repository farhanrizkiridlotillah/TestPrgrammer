<div class="-intro-x breadcrumb mr-auto">
    @php
        $breadcrumbs = \App\Helpers\Services\MenuHelpers::breadcrumbMenu();
        $previous_breadcrumb = null;
    @endphp
    @foreach ( $breadcrumbs as $breadcrumb)
        @if ($breadcrumb['status'] == true)
            @if ($breadcrumb == end($breadcrumbs))
            <a href="{{URL($breadcrumb['url'])}}" class="breadcrumb--active">{{$breadcrumb['name']}}</a> 
            {{-- <i data-feather="chevron-right" class="breadcrumb__icon"></i> --}}
            @else
            <a href="{{URL($breadcrumb['url'])}}">{{$breadcrumb['name']}}</a> 
            <i data-feather="chevron-right" class="breadcrumb__icon"></i>
            @endif
            @php
                $previous_breadcrumb = $breadcrumb;
            @endphp
        @else
            @if ($breadcrumb == end($breadcrumbs))
            <a href="{{URL($breadcrumb['url'])}}" class="breadcrumb--active">{{$previous_breadcrumb['name']}}</a> 
            {{-- <i data-feather="chevron-right" class="breadcrumb__icon"></i> --}}
            @endif
        @endif
    @endforeach 
</div>