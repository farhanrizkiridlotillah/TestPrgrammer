<nav class="side-nav">
    <ul>
        <li>
            <a href="" class="side-menu side-menu--{{ (\App\Helpers\Services\MenuHelpers::activeMenu('beranda') == 'active') ? 'bg-theme-31 text-white' : ''}}">
                <div class="side-menu__icon"> <i data-feather="home"></i> </div>
                <div class="side-menu__title">
                    Beranda 
                </div>
            </a>
        </li>
        <li class="side-nav__devider my-6"></li>
        <li>
            <a href="{{URL('/master/barang')}}" class="side-menu side-menu--{{ \App\Helpers\Services\MenuHelpers::activeMenu('barang')== 'active' ? 'bg-theme-31 text-white' : ''}}">
                <div class="side-menu__icon"> <i data-feather="coffee"></i> </div>
                <div class="side-menu__title"> Data Barang </div>
            </a>
        </li>
        <li class="side-nav__devider my-6"></li>
        <li>
            <a href="{{URL('/transaksi/pembelian-barang')}}" class="side-menu side-menu--{{ \App\Helpers\Services\MenuHelpers::activeMenu('pembelian-barang')== 'active' ? 'bg-theme-31 text-white' : ''}}">
                <div class="side-menu__icon"> <i data-feather="shopping-cart"></i> </div>
                <div class="side-menu__title"> Pembelian Barang </div>
            </a>
        </li>
        <li class="side-nav__devider my-6"></li>
    </ul>
</nav>