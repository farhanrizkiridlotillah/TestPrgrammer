<?php

use App\Http\Controllers\MasterBarangController;
use App\Http\Controllers\TransaksiPembelianController;
use App\Models\TransaksiPembelian;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('home');
});

Route::middleware(['web'])->prefix('master')->group(function () {
    Route::resource('/barang', MasterBarangController::class);
});
Route::middleware(['web'])->prefix('transaksi')->group(function () {
    Route::resource('/pembelian-barang', TransaksiPembelianController::class);
    Route::get('/data-barang', [TransaksiPembelianController::class, 'get_data_barang']);
});


